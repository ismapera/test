package com.example.demo.api;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.service.CommentsService;

@RestController
public class CommentsApi {
	
	@Autowired
	private CommentsService commentsService;
    
    
    @PostMapping(value="/comments")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
    	List<String> listComment= commentsService.getComment(file);

        return ResponseEntity.status(HttpStatus.OK).body(listComment);

    }
}