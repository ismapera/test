package com.example.demo.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.ParameterizedTypeReference;

import com.example.demo.dto.Comment;

@Service
public class CommentsService {
	
	private static final Logger log = LoggerFactory.getLogger(CommentsService.class);
	private static final String characterPipe="|";
	
	
    public List<String> getComment(MultipartFile file) throws IOException {
    	List<String> listResponse = new ArrayList<>();
    	String postId=null;
    	String id=null;
    	String email=null;
    	RestTemplate restTemplate = new RestTemplate();
        String url = getPropValues(file);
        System.out.println("url: "+url);
        ResponseEntity<List<Comment>> response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<Comment>>() {
        });
        List<Comment> todoList = response.getBody();
        if(null!=todoList && !todoList.isEmpty()) {
        	for (Iterator iterator = todoList.iterator(); iterator.hasNext();) {
				Comment comment = (Comment) iterator.next();
				postId = (null==comment.getPostId())?"":comment.getPostId().toString();
				id = (null==comment.getId())?"":comment.getId().toString();
				email = (null==comment.getEmail())?"":comment.getEmail().toString();
				listResponse.add(postId+characterPipe+id+characterPipe+email);
				System.out.println("nombre: "+comment.getName());
				
				
				
			}
        }
        return listResponse;
}
    
    
    
	public String getPropValues(MultipartFile file) throws IOException {
		String url = null;

		try {
			Properties prop = new Properties(); 
 
			if (null!=file && null!=file.getInputStream()) {
				prop.load(file.getInputStream());
			} else {
				throw new FileNotFoundException("Archivo de propiedades no encontrado");
			}
			url = prop.getProperty("url");
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			
		}
		return url;
	}

}
